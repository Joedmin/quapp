import 'package:flutter/material.dart';
import 'package:quapp/utils/constantss.dart';
import 'package:quapp/providers/quote_provider.dart';
import 'package:provider/provider.dart';
import 'package:quapp/utils/shared_prefs.dart';

import 'widgets/swipe_navigation.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SharedPrefs.init();

  runApp(
    ChangeNotifierProvider<QuoteProvider>(
      create: (context) => QuoteProvider(),
      child: const QuoteApp(),
    ),
  );
}

class QuoteApp extends StatelessWidget {
  const QuoteApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      color: Constants.backgroundColor,
      home: SwipeNavigation(),
    );
  }
}
