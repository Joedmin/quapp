import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:quapp/models/quote_model.dart';
import 'package:quapp/services/api_service.dart';
import 'package:quapp/utils/constantss.dart';
import 'package:quapp/utils/shared_prefs.dart';

class QuoteProvider extends ChangeNotifier {
  final ApiService _apiService = ApiService();
  final SharedPrefs preferences = SharedPrefs();

  QuoteModel? _quote;
  QuoteModel? get quote => _quote;

  QuoteProvider() {
    loadQuote();
  }

  Future<void> loadQuote() async {
    var loaded = preferences.getString(Constants.quoteKey);
    if (loaded != null) {
      updateAndNotify(QuoteModel.fromJson(jsonDecode(loaded)));
      return;
    }

    await fetchQuote();
  }

  // TODO: Why is it lagging on firt execution?
  Future<void> fetchQuote() async {
    updateAndNotify(null);

    try {
      updateAndNotify(await _apiService.fetchData());
      await preferences.setString(Constants.quoteKey, jsonEncode(_quote));
    } catch (e) {
      throw Exception('Failed to fetch data: $e');
    }
  }

  void updateAndNotify(QuoteModel? value) {
    _quote = value;
    notifyListeners();
  }
}
