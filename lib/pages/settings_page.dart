import 'package:flutter/material.dart';
import 'package:quapp/utils/constantss.dart';
import 'package:quapp/utils/shared_prefs.dart';
import 'package:settings_ui/settings_ui.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({super.key});

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  late bool notificationsEnabled;
  late TimeOfDay notificationTime;

  late SharedPrefs preferences = SharedPrefs();

  _SettingsPageState() {
    getPreferences();
    var timeString = preferences.getString(Constants.notificationTimeKey);
    notificationsEnabled =
        preferences.getBool(Constants.notificationsEnabledKey) ?? false;
    notificationTime = timeString != null
        ? TimeOfDay(
            hour: int.parse(timeString.split(":")[0]),
            minute: int.parse(timeString.split(":")[1]))
        : const TimeOfDay(hour: 8, minute: 0);
  }

  Future<void> getPreferences() async {
    preferences = SharedPrefs();
  }

  @override
  Widget build(BuildContext context) {
    return SettingsList(
      lightTheme: const SettingsThemeData(
        settingsListBackground: Colors.transparent,
        settingsSectionBackground: Colors.transparent,
        titleTextColor: Constants.navyBlue,
        settingsTileTextColor: Colors.black54,
      ),
      contentPadding: const EdgeInsets.all(9),
      sections: [
        SettingsSection(
          title: const Text("Bbbbb"),
          tiles: [
            SettingsTile(
              title: const Text("Tags"),
              description: const Text("Quote cathegories"),
              leading: const Icon(Icons.tag),
              onPressed: (context) => {
                // TODO: show tags - fetch them from api
              },
            ),
            SettingsTile.switchTile(
              title: const Text("Daily quotes"),
              description:
                  const Text("Send a daily notification with a random quote"),
              leading: const Icon(Icons.notifications),
              initialValue: notificationsEnabled,
              activeSwitchColor: Constants.navyBlue,
              onToggle: (bool value) {
                setState(() {
                  notificationsEnabled = value;
                  preferences.setBool(
                      Constants.notificationsEnabledKey, notificationsEnabled);
                });
              },
            ),
            SettingsTile(
              title: const Text("Time"),
              enabled: notificationsEnabled,
              leading: const Icon(Icons.alarm),
              description: const Text("Daily notification time"),
              trailing: Text(
                notificationTime.format(context),
                style: TextStyle(
                    color:
                        notificationsEnabled ? Colors.black87 : Colors.black26),
              ),
              onPressed: (context) => updateNotificationTime(context),
            ),
          ],
        ),
      ],
    );
  }

  Future updateNotificationTime(BuildContext context) async {
    var newNotificationTime = await showTimePicker(
      context: context,
      initialTime: notificationTime,
    );

    if (newNotificationTime != null) {
      setState(() {
        preferences.setString(
            Constants.notificationTimeKey, newNotificationTime.format(context));
        notificationTime = newNotificationTime;
      });
    }
  }
}
