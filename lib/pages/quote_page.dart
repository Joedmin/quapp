import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:quapp/providers/quote_provider.dart';
import 'package:quapp/widgets/quote_widget.dart';

class QuotePage extends StatefulWidget {
  const QuotePage({super.key});

  @override
  State<QuotePage> createState() => _QuotePageState();
}

class _QuotePageState extends State<QuotePage> {
  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      // TODO: Do I want two refresh indicators?
      // color: Colors.white,
      // backgroundColor: Colors.blue,
      onRefresh: () async =>
          Provider.of<QuoteProvider>(context, listen: false).fetchQuote(),
      notificationPredicate: (ScrollNotification notification) {
        return notification.depth == 0;
      },
      triggerMode: RefreshIndicatorTriggerMode.onEdge,
      child: SingleChildScrollView(
        physics: const AlwaysScrollableScrollPhysics(),
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.all(9.0),
          child: const Quote(),
        ),
      ),
    );
  }
}
