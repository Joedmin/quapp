import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs {
  static SharedPreferences? _sharedPrefs;
  static SharedPreferences get sharedPrefs {
    if (_sharedPrefs == null) {
      throw Exception('SharedPrefs not initialized!');
    }
    return _sharedPrefs!;
  }

  factory SharedPrefs() => _instance;

  SharedPrefs._internal();

  static final SharedPrefs _instance = SharedPrefs._internal();

  static Future<void> init() async {
    _sharedPrefs ??= await SharedPreferences.getInstance();
  }

  bool? getBool(String key) => sharedPrefs.getBool(key);
  int? getInt(String key) => sharedPrefs.getInt(key);
  double? getDouble(String key) => sharedPrefs.getDouble(key);
  String? getString(String key) => sharedPrefs.getString(key);

  Future<bool> setBool(String key, bool value) =>
      sharedPrefs.setBool(key, value);
  Future<bool> setInt(String key, int value) => sharedPrefs.setInt(key, value);
  Future<bool> setDouble(String key, double value) =>
      sharedPrefs.setDouble(key, value);
  Future<bool> setString(String key, String value) =>
      sharedPrefs.setString(key, value);

  Future<bool> remove(String key) => sharedPrefs.remove(key);
}

final sharedPrefs = SharedPrefs();
