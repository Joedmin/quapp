import 'package:flutter/material.dart';

class Constants {
  static const Color backgroundColor = Color.fromARGB(255, 245, 245, 245);
  static const Color navyBlue = Color.fromARGB(255, 0, 12, 102);

  static const quoteKey = "quote_key";
  static const notificationsEnabledKey = "notification_enabled";
  static const notificationTimeKey = "notification_time";
}
