import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:quapp/models/quote_model.dart';

class ApiService {
  static const String apiUrl = 'https://api.quotable.io/random?tags=';

  // TODO: tag settings
  var tags = ["inspirational", "knowledge", "philosophy", "self-help"];

  Future<QuoteModel> fetchData() async {
    try {
      // final response = await http.get(Uri.parse(apiUrl + tags.join(('|'))));
      final response = await http.get(Uri.parse(apiUrl));

      if (response.statusCode == 200) {
        return QuoteModel.fromJson(jsonDecode(response.body));
      }

      throw Exception('Failed to load data');
    } catch (e) {
      throw Exception('Failed to fetch data: $e');
    }
  }
}
