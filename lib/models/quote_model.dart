class QuoteModel {
  final String content;
  final String author;
  final List<String> tags;

  QuoteModel(this.content, this.author, this.tags);

  factory QuoteModel.fromJson(Map<String, dynamic> json) {
    return QuoteModel(
      json['content'] as String,
      json['author'] as String,
      (json['tags'] as List).map((item) => item as String).toList(),
    );
  }

  Map<String, dynamic> toJson() => {
        'content': content,
        'author': author,
        'tags': tags,
      };
}
