import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:quapp/providers/quote_provider.dart';

class Quote extends StatelessWidget {
  const Quote({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<QuoteProvider>(builder: (context, provider, _) {
      if (provider.quote == null) {
        return const SizedBox(
          width: 30,
          height: 30,
          child: Center(child: CircularProgressIndicator()),
        );
      }

      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            '„${provider.quote!.content}“',
            textAlign: TextAlign.center,
            style: const TextStyle(fontSize: 24),
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            provider.quote!.author,
            textAlign: TextAlign.center,
            style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          )
        ],
      );
    });
  }
}
