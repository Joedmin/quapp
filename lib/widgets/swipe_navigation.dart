import 'package:flutter/material.dart';
import 'package:quapp/pages/quote_page.dart';
import 'package:quapp/pages/saved_page.dart';
import 'package:quapp/pages/settings_page.dart';

class SwipeNavigation extends StatefulWidget {
  const SwipeNavigation({super.key});

  @override
  State<SwipeNavigation> createState() => _SwipeNavigationState();
}

class _SwipeNavigationState extends State<SwipeNavigation> {
  final PageController _pageController = PageController(
    initialPage: 1,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        children: const <Widget>[
          SettingsPage(),
          QuotePage(),
          SavedPage(),
        ],
      ),
    );
  }
}
